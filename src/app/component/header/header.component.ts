import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerTitle: string = 'Task Tracker';
  constructor() { }

  ngOnInit(): void {
  }

  toggleAddTask(){
    alert('toggle by event');
  }

}
