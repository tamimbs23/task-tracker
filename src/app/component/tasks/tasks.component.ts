import { Component, OnInit } from '@angular/core';
import { Task } from '../../Task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks: Task[] = [];

  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.taskService.getTasks().subscribe((x) => this.tasks = x );
  }

  onDelete(task: Task){
    this.taskService
    .onDeleteTask(task)
    .subscribe(() => (this.tasks 
      = this.tasks.filter((x) => x.id !== task.id))
      );
  }

  onToggleReminder(task: Task){
    task.reminder = !task.reminder;
    this.taskService
      .updateTaskReminder(task)
      .subscribe();
  }

  onAdd(task: Task){
    this.taskService
    .onAddTask(task)
    .subscribe((x) => this.tasks.push(task));
  }
}
